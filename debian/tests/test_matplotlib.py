#! /usr/bin/python3

# turing uses only Axes, FigureCanvasQTAgg, Figure, AutoLocator, LinearLocator
# however users of turing rely on all other matplotlib's functions,
# so matplotlib's own test suite is enough.

# making just some smoke...

from matplotlib.axes import Axes
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.ticker import AutoLocator, LinearLocator
