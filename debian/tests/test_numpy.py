#! /usr/bin/python3

import numpy as np

# turing uses only the function np.arange
# however users of turing rely on all other functions,
# so numpy's own test suite is enough.

# making just some smoke...

assert list(np.arange(0, 10, 2)) == [0, 2, 4, 6, 8]
assert list(np.arange(0, 10, np.pi)) == [0, np.pi, np.pi*2, np.pi*3]

